
# Savills-landing

The production folder named **"dist"**. It consists of forlders: libraries(libs), styles(css), scripts(js), images; and files(index and alternative).

* Installation
* Production build
* Technologies used  
* Development
* Structure 
    * Making changes
    * Header section
        * Swithing words
        * Slider
    * Apartments section
        * Apartment-item (popup)
            * Carousel
        * Filter
    * Popup facilities
    * Request popup

## Installation
Install all dependencies
```terminal
npm install
```
## Production build
The production build is located in the **dist** folder. Make sure you run this command before moving the content to the server.
```terminal
npm run build
```
Add the content of the **dist** folder to your server. 

## Technologies used
---
* HTML
* SCSS
* GULP builder
* JS, JQUERY

### Libraries
* slick - popup gallery
* splice - apartment item gallery
* jquery - interactions

# Development
---
## Structure

To change content you need to run in files located in the root folder:
- index.html
- alternative.html
- css (divided by sections)
- js (for scripts)
- libs

## Make changes
```terminal
npm start
```
After, it will open into browser window.  
- index.html: http://localhost:3000/dist/index.html
- alternative.html: http://localhost:3000/dist/alternative.html

### Header section 
---
All the functionality is placed in the **./js/header.js**. 

#### Swithing words
All the displayed texts are placed in the **texts** variable. So, you can add/change the existing texts.
```
const texts = ["скидка 2% до 1 марта", "Ипотека 7%"];
```

#### Slider
Change images you can in the header.scss file. They are made as a background-image. 
```scss
.header {
  ...
  &__bg {
    ...
    &--1 {
      background: linear-gradient(
          180deg,
          #84a1c8 13.12%,
          rgba(132, 161, 200, 0) 38.65%
        ),
        url(../../images/header/bg.jpg) #84a1c9;
      ...
      }
    &--2 {
      background: url(../../images/header/bg.jpg) #84a1c9;
      ...
    }
    &--3 {
      background: url(../../images/header/bg.jpg) #84a1c9;
      ...
    }
    &--4 {
      background: url(../../images/header/bg.jpg) #84a1c9;
      ...
    }
    &--5 {
      background: url(../../images/header/bg.jpg) #84a1c9;
      ...
    }
  }
    ...
}
```
The images for the slider are stored in the header folder.
```js
--images
    --header
        bg.jpg
        ...
```
To change the speed of the slider you can here.
```js
 if (currentSlide === 5) {
     ...
    } else {
      slick.options.autoplaySpeed = 5000; //  5 seconds
    }
```



### Apartments section
---
All the apartments functionality is placed in the **./js/apartments.js**
#### scructure

- items
- apartment items
- The apartment gallery carousel functionality
- show all apartments list
- filter

It handles a stack of items(apartments). 
```bash
const items = [
  {
    id: 0, // id of apartment
    type: "price", // sold | rent
    floor: 6, 
    rooms: 2,
    bathrooms: 2,
    housing: "1/2",  //корпус
    area: 82,
    price: 37753000,
    images: [1, 2, 1, 2], // images names
    centeredImages: [2], // centered images
  },
  ...
  ]
```

#### Apartment-item (popup)
This popup is a temolate, that is filled by the data of the opened item. Each apartment item has own carousel.

##### Carousel

Images are sorted by apartments id.
- carousel folder "./images/carousel/apartment-item/"
- id - apartment folder name
- images - image
- "./images/carousel/apartment-item/1/1.jpg"
```js
--images
    --carousel
        --apartment-item
            -- 0 // id
                 1.jpg
                 2.jpg
                 ...
            -- 1
            -- 2
            ...
```

### Filter

- area slider
- budget slider
- range slider (rooms amount)

The values could be taken from the parameter - **ui (ui.values)** from the slide function of a slider.
You can set the minimun and maximum values for all three range sliders. The "values" property sets the initial points between the min and max.
```js
 $("#rooms-range").slider({
      ...
      min: 0,
      max: 6,
      ...
      values: [3, 5],
      slide: function (e, ui) {
        $("#rooms-range-amount").html(ui.values[0] + " — " + ui.values[1]);
      },
      ...
  });
```

### Popup facilities
---
Items are managed in the html file. The item has image, text and subtext 
```html
 <div class="popup-facilities" id="popup-facilities">
     ...
      <div class="popup-facilities__wrap">
        ...
        <!--item-->
        <article class="popup-facilities__item">
          <svg class="icon icon-wind popup-facilities__image">
            <use xlink:href="./images/sprite.svg#icon-wind"></use>
          </svg>
          <h3 class="popup-facilities__text">Вентиляция и кондиционирование</h3>
          <p class="popup-facilities__subtext">
            Приточно-вытяжная вентиляция. Мультизональная система центрального
            кондиционирования типа VRF.
          </p>
        </article>
        ...
```
All the icons placed into a sprite.svg file. The names of the icons are placed in the **'./scss/utils/sprite.scss'** file. 
All icons the facility section uses:
* icon-wind 
* icon-o2 
* icon-shield-chekmark-yellow
* icon-swimming-pool
* icon-wind-blue-dark 
* icon-grid-red
* icon-books
* icon-wind-black
* icon-settings

Make sure to add the same class to svg tag, and same id to the href attribute.
```html
<svg class="... icon-wind ...">
    <use xlink:href="... #icon-wind"></use>
</svg>
```

### Request popup
---
This is a template popup. So it shows the elements related to the opened request form. All the buttons opening these forms have **data-type** attribute (data-type="excursion").
The **data-type** may be: 
* excursion
* call
* request
* request2 (has 2 inputs for phone and email)
* excursion
* apartment
* backcall
* brochure
* brochure2 (handles type of the number (telegram, whatsapp, message))

The inputs values are handled in the **handleSuccessButton** function. 
Inputs for the request2 form:
* #input-phone1
* #input-email1

Input for the brochure form:
* #input-email2

Input for other forms:
* #input-phone2

To read the value
```js
$("#input-phone2").val()
```

