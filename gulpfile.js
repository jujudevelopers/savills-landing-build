var gulp = require("gulp");
const { series } = require('gulp');
var sass = require("gulp-sass");
var autoprefixer = require("gulp-autoprefixer");
var browserSync = require("browser-sync").create();
var cssnano = require("gulp-cssnano");
var uglify = require("gulp-uglify");
var concat = require("gulp-concat");
const imagemin = require("gulp-imagemin");
const htmlmin = require("gulp-htmlmin");

function style() {
  return gulp
    .src("./scss/*.scss")
    .pipe(sass())
    .pipe(autoprefixer())
    .pipe(cssnano())
    .pipe(gulp.dest("./dist/css"))
    .pipe(browserSync.stream());
}

function markup() {
  return gulp
    .src("./*.html")
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(gulp.dest("./dist"))
    .pipe(browserSync.stream());
}

function images() {
  return gulp
    .src([
      "./images/**/*.jpg",
      "./images/**/*.png",
      "./images/**/*.svg",
      "./images/**/*.ico",
    ])
    .pipe(
      imagemin([
        imagemin.gifsicle({ interlaced: true }),
        imagemin.mozjpeg({ quality: 75, progressive: true }),
        imagemin.optipng({ optimizationLevel: 5 }),
      ])
    )
    .pipe(gulp.dest("./dist/images"))
    .pipe(browserSync.stream());
}

function scripts() {
  return gulp
    .src("./js/*.js")
    .pipe(concat("all.js"))
    .pipe(uglify())
    .pipe(gulp.dest("./dist/js"));
}

function libs() {
  return gulp.src("./libs/**/*.*").pipe(gulp.dest("./dist/libs"));
}

function watch() {
  browserSync.init({
    server: {
      baseDirL: "baseDir./",
    },
  });
  gulp.watch(
    [
      "./images/**/*.jpg",
      "./images/**/*.png",
      "./images/**/*.svg",
      "./images/**/*.icon",
    ],
    images
  );
  gulp.watch("./scss/**/*.scss", style);
  gulp.watch("./*.html", markup).on("change", browserSync.reload);
  gulp.watch("./js/**/*.js", scripts).on("change", browserSync.reload);
}

exports.style = style;
exports.scripts = scripts;
exports.images = images;
exports.markup = markup;
exports.watch = watch;
exports.libs = libs;
exports.build = series(markup, libs, images, style, scripts);
