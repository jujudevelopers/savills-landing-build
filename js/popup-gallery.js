let isPopupGalleryOpen = false;

$(function (e) {
  function galleryPopup(start = 1, gallerylength) {
    const startingSlide = gallerylength > start ? start - 1 : gallerylength - 1;
    // popup gallery
    const secondarySlider = new Splide("#popup-gallery-secondary-slider", {
      start: startingSlide,
      rewind: true,
      fixedWidth: 90,
      fixedHeight: 90,
      isNavigation: true,
      gap: 16,
      pagination: false,
      cover: true,
      arrows: false,
    }).mount();

    const primarySlider = new Splide("#popup-gallery-primary-slider", {
      start: startingSlide,
      width: "100%",
      autoWidth: true,
      height: "80vh",
      heightRatio: 0.5,
      arrows: false,
      perPage: 1,
      pauseOnHover: false,
      pauseOnFocus: false,
      resetProgress: false,
      pagination: false,
    });
    primarySlider.sync(secondarySlider).mount();
  }

  var galleryChildsLength = $(
    "#popup-gallery-primary-slider .splide__list"
  ).children().length;
  galleryPopup(0, galleryChildsLength);

  $(".masonry__image").click(function () {
    const clickedGalleryItem = $(this).attr("data-galleryItem");
    console.log(clickedGalleryItem);
    galleryPopup(clickedGalleryItem, galleryChildsLength);
    openGallery();
  });

  // open from header
  $("#popup-gallery-open-intro").click(function () {
    document.documentElement.style.overflow = "hidden";
    $("#popup-gallery").addClass("popup-gallery--show");
    isPopupGalleryOpen = true;
    centerPopupCarouselsChilds();
  });

  function openGallery() {
    document.documentElement.style.overflow = "hidden";
    $("#popup-gallery").addClass("popup-gallery--show");
    isPopupGalleryOpen = true;
    centerPopupCarouselsChilds();
  }
  // open from gallery(masonry) section
  $("#popup-gallery-open").click(function () {
    openGallery();
  });
  // close
  $("#popup-gallery-close").click(function () {
    document.documentElement.style.overflow = "auto";
    $("#popup-gallery").removeClass("popup-gallery--show");
    isPopupGalleryOpen = false;
  });

  // center items on resize
  $(window).resize(function () {
    centerPopupCarouselsChilds();
  });

  // center items
  function centerPopupCarouselsChilds() {
    if (isPopupGalleryOpen) {
      const popupGalleryWidth = $("#popup-gallery").width();
      const popupGalleryChilds = $(
        "#popup-gallery-secondary-slider-list"
      ).children().length;
      const popupGalleryItemsLength = 107 * popupGalleryChilds - 16; // elem. width + margin right
      if (popupGalleryWidth > popupGalleryItemsLength) {
        $("#popup-gallery-secondary-slider-track").addClass("track-center");
      } else {
        $("#popup-gallery-secondary-slider-track").removeClass("track-center");
      }
    }
  }
});
