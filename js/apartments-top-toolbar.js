$(function () {
  $window = $(window);

  $(window).scroll(function () {
    handleApartmentsTopToolbarChange();
  });
  $(window).resize(function () {
    handleApartmentsTopToolbarChange();
  });

  function handleApartmentsTopToolbarChange() {
    const apContainerWrapTop = $(".apartments__container-wrap").offset().top;
    const apFilterTop = $(".apartments__filter").offset().top;
    const apContainerWrapHeight = $(".apartments__container-wrap").height();
    const apContainerWrapBottom =
      apContainerWrapTop + apContainerWrapHeight - 300;

    if (apContainerWrapHeight > 400) {
      if (
        $window.scrollTop() >= apContainerWrapTop &&
        $window.scrollTop() < apContainerWrapBottom
      ) {
        $(".apartments-top-filter").css({ display: "grid" });
      }

      if (
        $window.scrollTop() <= apFilterTop ||
        $window.scrollTop() > apContainerWrapBottom
      ) {
        $(".apartments-top-filter").css({ display: "none" });
      }
    } else {
      $(".apartments-top-filter").css({ display: "none" });
    }
  }
});

