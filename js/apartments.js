// items
const items = [
  {
    id: 0,
    type: "price", // sold | rent
    floor: 6,
    rooms: 2,
    bathrooms: 2,
    housing: "1/2",
    area: 82,
    price: 37753000,
    images: [1, 2, 1, 2],
    centeredImages: [2],
  },
  {
    id: 1,
    type: "rent",
    floor: 3,
    rooms: 1,
    bathrooms: 2,
    housing: "2/2",
    area: 50,
    rent: 350000,
    images: [1, 2, 1, 2],
    centeredImages: [2],
  },
  {
    id: 2,
    type: "sold",
    floor: 6,
    rooms: 2,
    bathrooms: 2,
    housing: "1/2",
    area: 82,
    images: [1, 1, 2],
    centeredImages: [2],
  },
  {
    id: 3,
    type: "price",
    floor: 6,
    rooms: 2,
    bathrooms: 2,
    housing: "1/2",
    area: 82,
    price: 37753000,
    images: [1, 2, 1],
    centeredImages: [2],
  },
  {
    id: 4,
    type: "rent",
    floor: 6,
    rooms: 2,
    bathrooms: 2,
    housing: "1/2",
    area: 82,
    rent: 350000,
    images: [1, 2, 1],
    centeredImages: [2],
  },
  {
    id: 5,
    type: "sold",
    floor: 6,
    rooms: 2,
    bathrooms: 2,
    housing: "1/2",
    area: 82,
    images: [1, 2, 1],
    centeredImages: [2],
  },
  {
    id: 6,
    type: "sold",
    floor: 6,
    rooms: 2,
    bathrooms: 2,
    housing: "1/2",
    area: 82,
    images: [1, 2],
    centeredImages: [2],
  },
  {
    id: 7,
    type: "rent",
    floor: 6,
    rooms: 2,
    bathrooms: 2,
    housing: "1/2",
    area: 82,
    rent: 350000,
    images: [1, 2],
    centeredImages: [2],
  },
  {
    id: 8,
    type: "price",
    floor: 6,
    rooms: 2,
    bathrooms: 2,
    housing: "1/2",
    area: 82,
    price: 37753000,
    images: [1, 2],
    centeredImages: [2],
  },
  {
    id: 9,
    type: "price",
    floor: 6,
    rooms: 2,
    bathrooms: 2,
    housing: "1/2",
    area: 82,
    price: 37753000,
    images: [1, 2],
    centeredImages: [2],
  },
  {
    id: 10,
    type: "price", // sold | rent
    floor: 6,
    rooms: 2,
    bathrooms: 2,
    housing: "1/2",
    area: 82,
    price: 37753000,
    images: [1, 2, 1, 2],
    centeredImages: [2],
  },
  {
    id: 11,
    type: "rent",
    floor: 3,
    rooms: 1,
    bathrooms: 2,
    housing: "2/2",
    area: 50,
    rent: 350000,
    images: [1, 2, 1, 2],
    centeredImages: [2],
  },
  {
    id: 12,
    type: "sold",
    floor: 6,
    rooms: 2,
    bathrooms: 2,
    housing: "1/2",
    area: 82,
    images: [1, 1, 2],
    centeredImages: [2],
  },
  {
    id: 13,
    type: "price",
    floor: 6,
    rooms: 2,
    bathrooms: 2,
    housing: "1/2",
    area: 82,
    price: 37753000,
    images: [1, 2, 1],
    centeredImages: [2],
  },
  {
    id: 14,
    type: "rent",
    floor: 6,
    rooms: 2,
    bathrooms: 2,
    housing: "1/2",
    area: 82,
    rent: 350000,
    images: [1, 2, 1],
    centeredImages: [2],
  },
  {
    id: 15,
    type: "sold",
    floor: 6,
    rooms: 2,
    bathrooms: 2,
    housing: "1/2",
    area: 82,
    images: [1, 2, 1],
    centeredImages: [2],
  },
  {
    id: 16,
    type: "sold",
    floor: 6,
    rooms: 2,
    bathrooms: 2,
    housing: "1/2",
    area: 82,
    images: [1, 2],
    centeredImages: [2],
  },
  {
    id: 17,
    type: "rent",
    floor: 6,
    rooms: 2,
    bathrooms: 2,
    housing: "1/2",
    area: 82,
    rent: 350000,
    images: [1, 2],
    centeredImages: [2],
  },
  {
    id: 18,
    type: "price",
    floor: 6,
    rooms: 2,
    bathrooms: 2,
    housing: "1/2",
    area: 82,
    price: 37753000,
    images: [1, 2],
    centeredImages: [2],
  },
  {
    id: 19,
    type: "price",
    floor: 6,
    rooms: 2,
    bathrooms: 2,
    housing: "1/2",
    area: 82,
    price: 37753000,
    images: [1, 2],
    centeredImages: [2],
  },
  {
    id: 20,
    type: "price", // sold | rent
    floor: 6,
    rooms: 2,
    bathrooms: 2,
    housing: "1/2",
    area: 82,
    price: 37753000,
    images: [1, 2, 1, 2],
    centeredImages: [2],
  },
  {
    id: 21,
    type: "rent",
    floor: 3,
    rooms: 1,
    bathrooms: 2,
    housing: "2/2",
    area: 50,
    rent: 350000,
    images: [1, 2, 1, 2],
    centeredImages: [2],
  },
  {
    id: 22,
    type: "sold",
    floor: 6,
    rooms: 2,
    bathrooms: 2,
    housing: "1/2",
    area: 82,
    images: [1, 1, 2],
    centeredImages: [2],
  },
  {
    id: 23,
    type: "price",
    floor: 6,
    rooms: 2,
    bathrooms: 2,
    housing: "1/2",
    area: 82,
    price: 37753000,
    images: [1, 2, 1],
    centeredImages: [2],
  },
  {
    id: 24,
    type: "rent",
    floor: 6,
    rooms: 2,
    bathrooms: 2,
    housing: "1/2",
    area: 82,
    rent: 350000,
    images: [1, 2, 1],
    centeredImages: [2],
  },
  {
    id: 25,
    type: "sold",
    floor: 6,
    rooms: 2,
    bathrooms: 2,
    housing: "1/2",
    area: 82,
    images: [1, 2, 1],
    centeredImages: [2],
  },
  {
    id: 26,
    type: "sold",
    floor: 6,
    rooms: 2,
    bathrooms: 2,
    housing: "1/2",
    area: 82,
    images: [1, 2],
    centeredImages: [2],
  },
  {
    id: 27,
    type: "rent",
    floor: 6,
    rooms: 2,
    bathrooms: 2,
    housing: "1/2",
    area: 82,
    rent: 350000,
    images: [1, 2],
    centeredImages: [2],
  },
  {
    id: 28,
    type: "price",
    floor: 6,
    rooms: 2,
    bathrooms: 2,
    housing: "1/2",
    area: 82,
    price: 37753000,
    images: [1, 2],
    centeredImages: [2],
  },
  {
    id: 29,
    type: "price",
    floor: 6,
    rooms: 2,
    bathrooms: 2,
    housing: "1/2",
    area: 82,
    price: 37753000,
    images: [1, 2],
    centeredImages: [2],
  },
  {
    id: 30,
    type: "price", // sold | rent
    floor: 6,
    rooms: 2,
    bathrooms: 2,
    housing: "1/2",
    area: 82,
    price: 37753000,
    images: [1, 2, 1, 2],
    centeredImages: [2],
  },
  {
    id: 31,
    type: "rent",
    floor: 3,
    rooms: 1,
    bathrooms: 2,
    housing: "2/2",
    area: 50,
    rent: 350000,
    images: [1, 2, 1, 2],
    centeredImages: [2],
  },
  {
    id: 32,
    type: "sold",
    floor: 6,
    rooms: 2,
    bathrooms: 2,
    housing: "1/2",
    area: 82,
    images: [1, 1, 2],
    centeredImages: [2],
  },
  {
    id: 33,
    type: "price",
    floor: 6,
    rooms: 2,
    bathrooms: 2,
    housing: "1/2",
    area: 82,
    price: 37753000,
    images: [1, 2, 1],
    centeredImages: [2],
  },
  {
    id: 34,
    type: "rent",
    floor: 6,
    rooms: 2,
    bathrooms: 2,
    housing: "1/2",
    area: 82,
    rent: 350000,
    images: [1, 2, 1],
    centeredImages: [2],
  },
  {
    id: 35,
    type: "sold",
    floor: 6,
    rooms: 2,
    bathrooms: 2,
    housing: "1/2",
    area: 82,
    images: [1, 2, 1],
    centeredImages: [2],
  },
  {
    id: 36,
    type: "sold",
    floor: 6,
    rooms: 2,
    bathrooms: 2,
    housing: "1/2",
    area: 82,
    images: [1, 2],
    centeredImages: [2],
  },
  {
    id: 37,
    type: "rent",
    floor: 6,
    rooms: 2,
    bathrooms: 2,
    housing: "1/2",
    area: 82,
    rent: 350000,
    images: [1, 2],
    centeredImages: [2],
  },
  {
    id: 38,
    type: "price",
    floor: 6,
    rooms: 2,
    bathrooms: 2,
    housing: "1/2",
    area: 82,
    price: 37753000,
    images: [1, 2],
    centeredImages: [2],
  },
  {
    id: 39,
    type: "price",
    floor: 6,
    rooms: 2,
    bathrooms: 2,
    housing: "1/2",
    area: 82,
    price: 37753000,
    images: [1, 2],
    centeredImages: [2],
  },
  {
    id: 40,
    type: "price",
    floor: 6,
    rooms: 2,
    bathrooms: 2,
    housing: "1/2",
    area: 82,
    price: 37753000,
    images: [1, 2],
    centeredImages: [2],
  },
];
let chosenItemId = null; // opened item id
let isApartmentItemOpen = false;

// convert number
function convertNumber(value, seperator = "'") {
  let val = value / 1;
  const sep =
    seperator === "'" ? "<span class='filter__apostrophe'>'</span>" : " ";
  const formed = val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, sep);
  return formed;
}

const formItems = (items, quantity = 3) => {
  const newItems = [];
  for (let i = 0; i < quantity; i++) {
    const item = items.find((el) => el.id === i);
    if (item) {
      const labelTypeText =
        item.type === "price"
          ? `${convertNumber(item.price)} ₽`
          : item.type === "rent"
          ? `${convertNumber(item.rent)} ₽ / мес`
          : "Продана";

      const rooms = item.rooms > 1 ? `${item.rooms} комнаты` : "1 комнатa";

      const newItem = `<article class="apartments__item">
  <div class="apartments__item-top">
    <div class="apartments__item-left">
      <span class="apartments__item-meters">
        ${item.area} м<sup>2</sup>
      </span>
      <span class="apartments__item-floor">${item.floor} этаж</span>
    </div>
    <span class="apartments__item-rooms">${rooms}</span>
  </div>
  <div class="apartments__item-wrap"
    data-id="${item.id}"
  >
    <img
      src="./images/apartments/${item.id}.jpg"
      alt="switch"
      class="apartments__item-image"
    />
    <div class="apartments__label apartments__label--${item.type}">${labelTypeText}</div>
  </div>
</article>`;

      newItems.push(newItem);
    }
  }

  // print items to page
  $("#apartments-container").html(newItems);

  // apartments item opening
  $(".apartments__item-wrap").click(function (e) {
    document.documentElement.style.overflow = "hidden";
    const id = $(e.currentTarget).data("id");
    chosenItemId = id;
    isApartmentItemOpen = true;
    const item = items.find((el) => el.id === id);

    if (item) {
      if(item.type === "sold") {
        $("#apartment-item__form").css("display", "none");
        $("#apartment-item-status").css("display", "block");
      } else {
        $("#apartment-item__form").css("display", "block");
        $("#apartment-item-status").css("display", "none");
      }
      $(".apartment-item").addClass("apartment-item--active");
      setCarousel(item);
      $("#apartment-rooms").html(item.rooms);
      $("#apartment-floor").html(item.floor);
      $("#apartment-housing").html(item.housing);
      $("#apartment-area").html(item.area);
      $("#apartment-bathrooms").html(item.bathrooms);
      if (item.type !== "sold") {
        const apPrice = convertNumber(item[item.type], " ");
        const newPrice = `${apPrice} ₽`;
        $("#apartment-price").html(newPrice);
      } else {
        $("#apartment-price").html("");
      }

      $(".apartment-item__close").click(function () {
        document.documentElement.style.overflow = "auto";
        chosenItemId = null;
        isApartmentItemOpen = false;
        $(".apartment-item").removeClass("apartment-item--active");
      });
  
      centerCarouselsChilds();
    }
  });
};

// center carousels childs if a few of them
function centerCarouselsChilds() {
  if (isApartmentItemOpen) {
    const apartmentItemGallery = $(".apartment-item-gallery").width();
    const countApItems = $("#apartment-thumbs").children().length;
    const apartmentItemsLength = 107 * countApItems - 16; // elem. width + margin right
    console.log(apartmentItemGallery, apartmentItemsLength);
    if (apartmentItemGallery > apartmentItemsLength) {
      $("#apartment-item-secondary-slider-track").addClass("track-center");
    } else {
      $("#apartment-item-secondary-slider-track").removeClass("track-center");
    }
  }
}

// initial apartments shown
const setDefault = (windowWidth) => {
  const initQuantity = windowWidth < 1000 && windowWidth >= 768 ? 2 : 3; // 2 - tablet, 3 - mobile, desktop
  formItems(items, initQuantity);
  $("#apartments-open").removeClass("more--hide");
};

let currentWidth = null;
// open all apartments
$(function (e) {
  const windowWidth = $(window).width();
  if (currentWidth !== windowWidth) {
    setDefault(windowWidth);
    currentWidth = windowWidth;
  }

  // show all appartments button
  $("#apartments-open").click(function () {
    formItems(items, items.length + 1);
    $("#apartments-open").addClass("more--hide");
  });
});

// apartments-item-carousel
function setCarousel(item) {
  let images = [];
  let thumbs = [];

  for (let i = 0; i < item.images.length; i++) {
    const name = item.images[i];
    const folder = item.id;
    const ifCentered = item.centeredImages.includes(name);

    const image = `<li class="splide__slide">
    <div
      class="splide__image${ifCentered ? " splide__image--centered" : ""}"
      style="
        background-image: url(./images/carousel/apartment-item/${folder}/${name}.jpg);
      "
    ></div>
  </li>`;
    images.push(image);

    const thumb = `<li class="splide__slide">
  <img src="./images/carousel/apartment-item/${folder}/${name}.jpg" alt="room" />
</li>`;
    thumbs.push(thumb);
  }

  $("#apartment-images").html(images);
  $("#apartment-thumbs").html(images);

  var secondarySlider = new Splide("#apartment-item-secondary-slider", {
    rewind: true,
    fixedWidth: 90,
    fixedHeight: 90,
    isNavigation: true,
    gap: 16,
    pagination: false,
    cover: true,
    arrows: false,
  }).mount();

  var primarySlider = new Splide("#apartment-item-primary-slider", {
    width: "100%",
    autoWidth: true,
    height: 416,
    heightRatio: 0.5,
    arrows: false,
    perPage: 1,
    pauseOnHover: false,
    pauseOnFocus: false,
    resetProgress: false,
    pagination: false,
    breakpoints: {
      640: {
        height: "80vh",
      },
    },
  });
  primarySlider.sync(secondarySlider).mount();
}

// on resize
$(window).resize(function () {
  const windowWidth = $(window).width();
  if (currentWidth !== windowWidth) {
    setDefault(windowWidth); // apartments to show
    currentWidth = windowWidth;
  }
  centerCarouselsChilds(); // center carousels childs
});

// filter
$(function (e) {
  function numbers(values, id) {
    const html = `${convertNumber(values[0])} — ${convertNumber(values[1])}`;

    $(id).html(html);
  }

  numbers([100, 3000], "#square-range-amount");
  numbers([300000, 90000000], "#budget-range-amount");

  $("*[data-openFilter]").click(function () {
    document.documentElement.style.overflow = "hidden";
    $("#filter").addClass("filter--active");

    $("#filter-close").click(function () {
      closeFilter();
    });
    $("#filter-close2").click(function () {
      closeFilter();
    });
    // close filter
    function closeFilter() {
      document.documentElement.style.overflow = "auto";
      $("#filter").removeClass("filter--active");
    }

    // area slider
    $("#square-range").slider({
      range: true,
      min: 100,
      max: 3000,
      values: [100, 3000],
      slide: function (e, ui) {
        numbers(ui.values, "#square-range-amount");
      },
    });
    // budget slider
    $("#budget-range").slider({
      range: true,
      min: 300000,
      max: 90000000,
      values: [300000, 70000000],
      slide: function (e, ui) {
        numbers(ui.values, "#budget-range-amount");
      },
    });
    // range slider
    $("#rooms-range").slider({
      animate: "fast",
      range: true,
      min: 0,
      max: 6,
      step: 1,
      values: [3, 5],
      slide: function (e, ui) {
        $("#rooms-range-amount").html(ui.values[0] + " — " + ui.values[1]);
      },
      create: function (e, ui) {
        e.preventDefault();
        setSliderTicks(e.target);
      },
    });

    // range dots
    function setSliderTicks(el) {
      var $slider = $(el);
      var max = $slider.slider("option", "max");
      var min = $slider.slider("option", "min");
      var spacing = 100 / (max - min);

      $slider.find(".ui-slider-tick-mark").remove();
      for (let i = 0; i < max - min; i++) {
        $('<span class="ui-slider-tick-mark"></span>')
          .css("right", spacing * i + "%")
          .appendTo($slider);
      }
    }
  });
});
