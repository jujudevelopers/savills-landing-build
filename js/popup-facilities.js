$(function (e) {
  // open facilities (удобства)
  $("#popup-facilities-open").click(function () {
    document.documentElement.style.overflow = "hidden";
    $("#popup-facilities").addClass( "popup-facilities--show" );
  });
  // close
  $("#popup-facilities-close").click(function () {
    document.documentElement.style.overflow = "auto";
    $("#popup-facilities").removeClass( "popup-facilities--show" );
  });
});
