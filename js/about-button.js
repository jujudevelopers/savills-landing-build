var aboutButton = document.querySelector("#about-button");

// show more text on mobile
aboutButton.addEventListener("click", () => {
  const aboutTextBg = document.querySelector(".about__text-bg");
  const aboutText = document.querySelector(".about__text");

  document.querySelector("#about-button").style.display = "none";
  aboutTextBg.style.display = "none";
  aboutText.style.height = "auto";
});
