$(function (e) {
  let currentText = 0;
  const texts = ["скидка 2% до 1 марта", "Ипотека 7%"];

  setInterval(() => {
    $(".header__text-bg").css({
      left: "auto",
      right: 0,
      width: "0%",
    });
    $("#header-text").text(texts[currentText]);

    if (texts.length - 1 === currentText) {
      currentText = 0;
    } else {
      currentText++;
    }

    setTimeout(() => {
      $(".header__text-bg").css({
        right: "auto",
        left: 0,
        width: "100%",
      });
    }, 4700);
  }, 5000);

  const slider = $("#header-slideshow").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    draggable: true,
    // autoplay: true,
    autoplaySpeed: 2000,
    pauseOnHover: false,
    pauseOnFocus: false,
    pauseOnDotsHover: false,
    swipe: true
  });

  $("#header-slideshow").on("touchend", (e) => {
    $("#header-slideshow").slick("slickPlay");
  });

  slider.on("afterChange", function (e, slick, currentSlide) {
    $("#header-slide-current").text(currentSlide + 1);

    if (currentSlide === 5) {
      slick.options.autoplaySpeed = 5000;
    } else {
      slick.options.autoplaySpeed = 5000;
    }
  });
});
