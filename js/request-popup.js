const request = $(".request");
const galleryButton = $("#gallery-button");
const detailsButton = $("#details-button");
const requestBeforewordsRequest2 = $(".request__beforewords--request2");
const requestBeforewordsBrochure2 = $(".request__beforewords--brochure2");
const requestAfterwordsCall = $(".request__afterwords--call");
const requestAfterwordsEmail = $(".request__afterwords--email");
const headerButtonRequest = $("#header-button-request");
// const detailsButtonBackCall = $(
//   "#details-button-backcall"
// );
const contactsButtonCall = $("#contacts__button--call");
const benefitsButton = $("#benefits-button");
const inputPhone = $(".request__input--phone");
const inputEmail = $(".request__input--email");
const bRequest = $("#button-request");
const bRequest2 = $("#button-request2");
const bBrochure2 = $("#button-brochure2");
const requestGroup = $(".request__group");
const requestBody = $(".request__body");
const requestToolbarBrochure2 = $("#request-toolbar-brochure2");

let currentFormType = null;
let isSuccessRequest = false;
// book apartment success
$("#book-apartment").click(() => {
  isSuccessRequest = true;
  const bookApartment = $("#book-apartment-input");

  console.log(bookApartment.val());
  console.log("opened item: ", chosenItemId);

  $("#request-success").css({ display: "block" });
  request.addClass("request--show");
  $(".request__container").css({ display: "none" });
});

// open modal
const handleClick = (rtype) => {
  currentFormType = rtype;
  document.documentElement.style.overflow = "hidden";

  request.addClass("request--show");
  $(".request__container").css({ display: "block" });

  $(`.request__text--${currentFormType}`).addClass("request-content--show");
  $(`.request__button--${currentFormType}`).addClass("request-content--show");

  if (["apartment", "call", "backcall", "request"].includes(currentFormType)) {
    requestAfterwordsCall.addClass("request-content--show");
  }
  if (
    [
      "request",
      "apartment",
      "call",
      "backcall",
      "request",
      "excursion",
      "brochure2",
    ].includes(currentFormType)
  ) {
    inputPhone.addClass("request-content--show");
  }
  if (["brochure"].includes(currentFormType)) {
    inputEmail.addClass("request-content--show");
    requestAfterwordsEmail.addClass("request-content--show");
  }
  if (["request2"].includes(currentFormType)) {
    requestGroup.addClass("request__group--show");
    requestBeforewordsRequest2.addClass("request__group--show");
  }
  if (["brochure2"].includes(currentFormType)) {
    requestBeforewordsBrochure2.addClass("request__group--show");
    requestBody.addClass("request__body--brochure2");
    inputPhone.addClass("request__input--brochure2");
    requestToolbarBrochure2.addClass("request-toolbar--show");
  }

  $(`.request__button--${currentFormType}`).click((e) => {
    handleSuccessButton(e);
    hideRequiredFormItems();
    $(".request__success").css({ display: "block" });
    $(".request__container").css({ display: "none" });
    isSuccessRequest = true;
  });
};

// hide modals items 
function hideRequiredFormItems() {
  if (
    ["apartment", "call", "backcall", "excursion", "request"].includes(
      currentFormType
    )
  ) {
    requestAfterwordsCall.removeClass("request-content--show");
  }
  if (
    [
      "apartment",
      "call",
      "backcall",
      "request",
      "brochure2",
      "request",
    ].includes(currentFormType)
  ) {
    inputPhone.removeClass("request-content--show");
  }
  if (["brochure"].includes(currentFormType)) {
    inputEmail.removeClass("request-content--show");
    requestAfterwordsEmail.removeClass("request-content--show");
  }
  if (["request2"].includes(currentFormType)) {
    requestGroup.removeClass("request__group--show");
    requestBeforewordsRequest2.removeClass("request__group--show");
  }
  if (["brochure2"].includes(currentFormType)) {
    requestBeforewordsBrochure2.removeClass("request__group--show");
    requestBody.removeClass("request__body--brochure2");
    inputPhone.removeClass("request__input--brochure2");
    requestToolbarBrochure2.removeClass("request-toolbar--show");
  }
  $(`.request__text--${currentFormType}`).removeClass("request-content--show");
  $(`.request__button--${currentFormType}`).removeClass(
    "request-content--show"
  );
}

// successful button 
function handleSuccessButton(event) {
  const buttonText = $(event.target).text();
  console.log("button: ", buttonText);
  console.log("chosenItemId: ", chosenItemId);
  console.log(
    "item: ",
    JSON.stringify(items.find((el) => el.id === chosenItemId))
  );

  if (
    [
      "request",
      "apartment",
      "call",
      "backcall",
      "request",
      "excursion",
      "brochure2",
    ]
  ) {
    console.log("phone 1", $(inputPhone).val());
  }
  if (["brochure"].includes(currentFormType)) {
    console.log("email 1", $(requestAfterwordsEmail).val());
  }

  if (["request2"].includes(currentFormType)) {
    console.log("phone 2", $("#input-phone2").val());
    console.log("email 2", $("#input-email2").val());
  }
  if (["brochure2"].includes(currentFormType)) {
    console.log("phone 2", $("#input-phone2").val());
    console.log("email 2", $("#input-email2").val());
  }
}

// handle buttons to open modals
galleryButton.click(function (e) {
  const rtype = e.target.dataset.type;
  handleClick(rtype);
});

contactsButtonCall.click(function (e) {
  const rtype = e.target.dataset.type;
  handleClick(rtype);
});

detailsButton.click(function (e) {
  const rtype = e.target.dataset.type;
  handleClick(rtype);
});

// detailsButtonBackCall.click(function (e) {
//   type = e.target.dataset.type;
//   handleClick(type);
// });

benefitsButton.click(function (e) {
  const rtype = e.target.dataset.type;
  handleClick(rtype);
});
bRequest.click(function (e) {
  const rtype = e.target.dataset.type;
  handleClick(rtype);
});
bRequest2.click(function (e) {
  const rtype = e.target.dataset.type;
  handleClick(rtype);
});
bBrochure2.click(function (e) {
  const rtype = e.target.dataset.type;
  handleClick(rtype);

  let activeBrochure2Type = "message";
  // brochure2 toolbar
  $(".request-toolbar__item").click((e) => {
    $(
      `.request-toolbar__item[data-brochure=${activeBrochure2Type}`
    ).removeClass("request-toolbar__item--active");
    console.log(e.target);
    activeBrochure2Type = e.target.dataset.brochure;

    $(`.request-toolbar__item[data-brochure=${activeBrochure2Type}]`).addClass(
      "request-toolbar__item--active"
    );
  });
});
if (headerButtonRequest) {
  headerButtonRequest.click(function (e) {
    const rtype = e.target.dataset.type;
    handleClick(rtype);
  });
}

// close modal
$("#close").click(function () {
  if (isSuccessRequest) {
    isSuccessRequest = false;
    $(".request__success").css({ display: "none" });
  } else {
    hideRequiredFormItems();
  }
  currentFormType = null;

  request.removeClass("request--show");
  if (!chosenItemId) {
    document.documentElement.style.overflow = "auto";
  }
});
