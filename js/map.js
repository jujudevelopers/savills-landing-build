//Map
ymaps.ready(init);
function init() {
  var myMap = new ymaps.Map("map", {
      center: [55.766785, 37.555786],
      zoom: 16,
      controls: ["zoomControl", "fullscreenControl"],
      beheviors: ["ruler"],
    }),
    myPlacemark = new ymaps.Placemark(
      [55.766785, 37.555786],
      {},
      {
        iconLayout: "default#image",
        iconImageHref: "../images/icons/pin-map.svg",
        iconImageSize: [32, 32],
      }
    );
  myMap.geoObjects.add(myPlacemark);
  myMap.behaviors.disable("scrollZoom");
}
