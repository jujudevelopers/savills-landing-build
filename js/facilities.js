function determineHorizontalScroll() {
  const windowWidth1 = $(window).width();

  var fcontainerWidth = $(".facilities__container").width();

  if (windowWidth1 > 1440) {
    var fdItems = $(".facilities__item").length;
    const fdItemWidth = (fdItems * 228) - 48;
    if (windowWidth1 > fcontainerWidth) {
      $(".facilities__container").addClass("facilities__container--center");
    } else {
      $(".facilities__container").removeClass("facilities__container--center");
    }
  } else {
    $(".facilities__container").removeClass("facilities__container--center");
  }
}

$(function (e) {
  determineHorizontalScroll();
});

$(window).resize(function () {
  determineHorizontalScroll();
});
